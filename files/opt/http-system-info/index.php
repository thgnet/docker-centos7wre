<?php

$image_name = "PHP 7.2 Web Runtime Environment";

$sys_name = getenv("RUNTIME_NAME");
$sys_version = trim(file_get_contents("/opt/VERSION"));

$page = "index";

if ($_SERVER['QUERY_STRING'] == "phpinfo") {
  phpinfo();
  exit();
}

$with_opcache = false;
if (function_exists("opcache_get_status"))
  $with_opcache = true;

$with_apc = false;
if (function_exists("apc_cache_info"))
  $with_apc = true;

$sys_uptime = shell_exec("uptime");

?>
<!DOCTYPE html>
<html>
<head>
  <style type="text/css">
body {
  font-family: sans-serif;
}
h1 a {
  color: inherit;
  text-decoration: none;
}
p.name {
  position: absolute;
  top: 1em;
  right: 1em;
  margin: 0;
  padding: 0.5em;
  border: 1px solid #000;
  font-family: monospace;
}
  </style>
</head>
<body>

<?php if ($page == "index"): ?>

<h1><a href=".">ThGnet <?= $image_name; ?> v<?= $sys_version; ?></a></h1>
<?php if ($sys_name != ""): ?><p class="name"><?= $sys_name; ?></p><?php endif; ?>

<p><?= $sys_uptime; ?></p>

<ul>
  <li><a href="server-info">server info</a></li>
  <li><a href="server-status">server status</a></li>
<?php if ($with_opcache): ?>
  <li><a href="mod_opcache.php">opcache status</a></li>
<?php else: ?>
  <li><span>opcache status</span> (not loaded)</li>
<?php endif; ?>
<?php if ($with_apc): ?>
  <li><a href="mod_apc.php">APC status</a></li>
<?php else: ?>
  <li><span>APC status</span> (not loaded)</li>
<?php endif; ?>
  <li><a href="?phpinfo">PHP info</a></li>
</ul>

<?php endif; ?>
</body>
</html>
