<?php

define("CFG_MEMCACHE_HOST", "172.17.0.1");
define("CFG_MEMCACHE_PORT", 11211);

header("Content-Type: text/plain");

$memcache = new Memcache();

$retval = $memcache->connect(CFG_MEMCACHE_HOST, CFG_MEMCACHE_PORT);
var_dump($retval);

$version = $memcache->getVersion();
echo "Server's version: " . $version . "\n";

$a = array("a0", "k" => "a1", "w" => "b2");

$retval = $memcache->set('mytestkey000', $a, false, 100);
die("Failed to save data at the server");

