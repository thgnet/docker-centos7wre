<html>
<head>
  <style type="text/css">

.right {
  text-align: right;
}

body {
  font-family: "Calibri", sans-serif;
  font-size: 9pt;
}
table.info {
  font-size: 9pt;
}
  table.info th {
    text-align: left;
    background-color: #3399cc;
    color: #ffffff;
    font-weight: bold;
    padding: 0.2em 1.2ex 0.2em 0.6ex;
    cursor: default;
  }
    table.info tr:hover th {
      background-color: #44aadd;
    }
    table.info tr.even th {
      background-color: #006699;
    }
    table.info tr.even:hover th {
      background-color: #1177aa;
    }

  /* hide the label */
  table.info th + td {
    display: none;
  }
  table.info tr.even td {
    background-color: #e0e0e0;
  }

table.details {
}
  table.details tr:hover td {
    background-color: #e0e0e0;
  }
  table.details td {
    background-color: #f4f4f4;
    padding: 1px 4px;
  }
  table.details td.entry {
    font-size: 8pt;
    font-family: "Courier New", monospace;
  }
  </style>
</head>
<body>

<?php

$type = (isset($_GET['type']) ? $_GET['type'] : 'system');
$order = (isset($_GET['order']) ? $_GET['order'] : 'default');

$info = apc_cache_info($type);

#header("Content-Type: text/plain"); var_dump($info); exit();

$reqs = $info['num_hits'] + $info['num_misses'];

function trunc_left($str, $length) {
  if (strlen($str) <= $length)
    return $str;

  return "..." . substr($str, -($length - 3));
}

function trunc_right($str, $length) {
  if (strlen($str) <= $length)
    return $str;

  return substr($str, 0, ($length - 3)) . "...";
}

/* apply an ordering to the records */
if (($order != 'default') && (count($info['cache_list']) > 1)) {
  if (!isset($info['cache_list'][0][$order]))
    die("Invalid ordering field \"$order\"");

  usort($info['cache_list'], function($a, $b) use ($order) {
      if (is_string($a[$order]))
        return strcmp($a[$order], $b[$order]);


      if ($a[$order] == $b[$order])
        return 0;

      return ($a[$order] < $b[$order] ? 1 : -1);
    });
}

$reorder_url = $_SERVER['PHP_SELF'] . '?' .
    'type=' . $type . '&';

?>

<p>
  View APC cache type: [
  <?php if ($type != "system"): ?><a href="<?php echo $_SERVER['PHP_SELF']; ?>?type=system">system</a><?php else: ?>system<?php endif; ?> |
  <?php if ($type != "user"): ?><a href="<?php echo $_SERVER['PHP_SELF']; ?>?type=user">user</a><?php else: ?>user<?php endif; ?> ]
</p>

<table class="info">
  <tr class="odd">
    <th>Number of slots</th>
    <td>num_slots</td>
    <td><?php echo $info['num_slots']; ?></td>
  </tr>
  <tr class="even">
    <th>Default TTL</th>
    <td>ttl</td>
    <td><?php echo $info['ttl']; ?></td>
  </tr>
  <tr class="odd">
    <th>Hits</th>
    <td>num_hits</td>
    <td><?php echo $info['num_hits']; ?> (<?php echo round($info['num_hits'] / $reqs * 100, 1); ?>%)</td>
  </tr>
  <tr class="even">
    <th>Misses</th>
    <td>num_misses</td>
    <td><?php echo $info['num_misses']; ?> (<?php echo round($info['num_misses'] / $reqs * 100, 1); ?>%)</td>
  </tr>
  <tr class="odd">
    <th>Inserts</th>
    <td>num_inserts</td>
    <td><?php echo $info['num_inserts']; ?></td>
  </tr>
  <tr class="even">
    <th>Expunges</th>
    <td>expunges</td>
    <td><?php echo $info['expunges']; ?></td>
  </tr>
  <tr class="odd">
    <th>Start time</th>
    <td>start_time</td>
    <td><?php echo strftime("%Y-%m-%d %H:%I:%S", $info['start_time']); ?></td>
  </tr>
  <tr class="even">
    <th>Memory size</th>
    <td>mem_size</td>
    <td><?php echo round($info['mem_size'] / 1024 / 1024, 2); ?> MiB</td>
  </tr>
  <tr class="odd">
    <th>Number of entries</th>
    <td>num_entries</td>
    <td><?php echo $info['num_entries']; ?></td>
  </tr>
  <tr class="even">
    <th>File upload progress</th>
    <td>file_upload_progress</td>
    <td><?php echo $info['file_upload_progress']; ?></td>
  </tr>
  <tr class="odd">
    <th>Memory type</th>
    <td>memory_type</td>
    <td><?php echo $info['memory_type']; ?></td>
  </tr>
  <tr class="even">
    <th>Locking type</th>
    <td>locking_type</td>
    <td><?php echo $info['locking_type']; ?></td>
  </tr>
</table>

<table class="details">
  <tr>
    <!-- <th>Type</th> -->
    <!-- <th>Device</th> -->
    <th><a href="<?php echo htmlspecialchars($reorder_url); ?>order=entry">Entry</a></th>
    <th><a href="<?php echo htmlspecialchars($reorder_url); ?>order=num_hits">Hits</a></th>
    <th><a href="<?php echo htmlspecialchars($reorder_url); ?>order=mtime">mtime</a></th>
    <th><a href="<?php echo htmlspecialchars($reorder_url); ?>order=creation_time">creation time</a></th>
    <th><a href="<?php echo htmlspecialchars($reorder_url); ?>order=deletion_time">deletion time</a></th>
    <th><a href="<?php echo htmlspecialchars($reorder_url); ?>order=access_time">access time</a></th>
    <th><a href="<?php echo htmlspecialchars($reorder_url); ?>order=ref_count">Ref count</a></th>
    <th><a href="<?php echo htmlspecialchars($reorder_url); ?>order=mem_size">Memory size</a></th>
  </tr>
<?php foreach ($info['cache_list'] as $entry): ?>
  <tr>
    <!-- <td><?php echo $entry['type']; ?></td> -->
    <!-- <td><?php echo $entry['device']; ?></td> -->
<?php if ($entry['type'] == 'file'): ?>
    <td class="entry"><?php echo trunc_left($entry['filename'], 50); ?></td>
<?php elseif ($entry['type'] == 'user'): ?>
    <td class="entry"><?php echo trunc_right($entry['info'], 50); ?></td>
<?php endif; ?>
    <td class="right"><?php if ($info['num_hits'] > 0): ?><small style="font-size: 0.6em;">(<?php echo round($entry['num_hits'] / $info['num_hits'] * 100, 1); ?>%)</small> <?php endif; ?><?php echo $entry['num_hits']; ?></td>
    <td><?php echo strftime("%Y-%m-%d %H:%I:%S", $entry['mtime']); ?></td>
    <td><?php echo strftime("%Y-%m-%d %H:%I:%S", $entry['creation_time']); ?></td>
    <td><?php if ($entry['deletion_time']) echo strftime("%Y-%m-%d %H:%I:%S", $entry['deletion_time']); ?></td>
    <td><?php echo strftime("%Y-%m-%d %H:%I:%S", $entry['access_time']); ?></td>
    <td class="right"><?php echo $entry['ref_count']; ?></td>
    <td class="right"><?php echo round($entry['mem_size'] / 1024, 2); ?> KiB</td>
  </tr>
<?php endforeach; ?>
</table>
