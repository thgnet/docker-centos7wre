<?php

$image_name = "PHP 7.2 Web Runtime Environment";

$sys_name = getenv("RUNTIME_NAME");
$sys_version = trim(file_get_contents("/opt/VERSION"));

$status = opcache_get_status();

$status['memory_usage']['total_memory'] =
    $status['memory_usage']['used_memory'] +
    $status['memory_usage']['free_memory'] +
    $status['memory_usage']['wasted_memory'];

$status['memory_usage']['free_percentage'] =
    (float) $status['memory_usage']['free_memory'] /
            $status['memory_usage']['total_memory'];

if ($_SERVER['QUERY_STRING'] == "raw") {
  header("Content-Type: text/plain");
  var_dump($status);
  exit();
}

function _green($text) {
  return "<span style=\"color: green; font-weight: bold;\">" . htmlspecialchars($text) . "</span>";
}

function _red($text) {
  return "<span style=\"color: red; font-weight: bold;\">" . htmlspecialchars($text) . "</span>";
}

function _fmt_num($value) {
  return number_format($value, 0, '', "'");
}

function _fmt_timestamp($value) {
  $retval = date("Y-m-d H:i:s", $value);
  return $retval;
}

function _fmt_mib($value, $units = true) {
  $fvalue = (float) $value / 1024 / 1024;
  $retval = "<span title=\"" . $value . " bytes\">" .
            number_format($fvalue, 1, '.', '') .
            ($units ? " MiB" : "") . "</span>";
  return $retval;
}

function _fmt_kib($value, $units = true) {
  $fvalue = (float) $value / 1024;
  $retval = "<span title=\"" . $value . " bytes\">" .
            number_format($fvalue, 1, '.', '') .
            ($units ? " kiB" : "") . "</span>";
  return $retval;
}

function _fmt_perc($value) {
  return round($value * 100) . "%";
}

function _sort_helper($col, $default_dir) {
  global $sort, $sort_dir;
  if ($sort == $col)
    return "?sort=$col," . ($sort_dir == 'asc' ? 'desc' : 'asc');
  return "?sort=$col,$default_dir";
}

$stats = $status['opcache_statistics'];
$scripts = $status['scripts'];

$sort = (string) (isset($_GET['sort']) ? $_GET['sort'] : null);
$sort_dir = 'asc';
if ($sort != "") {
  if (!preg_match('/^([a-z0-9_]+)(?:,(asc|desc))?$/', $sort, $regp))
    die("Bad sort value syntax");

  $sort = $regp[1];
  if (!empty($regp[2]))
    $sort_dir = $regp[2];
}

$_fn_sort = function($a, $b) use ($sort) {
  if ($sort == "hits") {
    if ($a['hits'] != $b['hits'])
      return ($a['hits'] < $b['hits'] ? -1 : 1);
    return strcmp($a['full_path'], $b['full_path']);
  }

  if ($sort == "memory") {
    if ($a['memory_consumption'] != $b['memory_consumption'])
      return ($a['memory_consumption'] < $b['memory_consumption'] ? -1 : 1);
    return strcmp($a['full_path'], $b['full_path']);
  }

  if ($sort == "atime") {
    if ($a['last_used_timestamp'] != $b['last_used_timestamp'])
      return ($a['last_used_timestamp'] < $b['last_used_timestamp'] ? -1 : 1);
    if ($a['hits'] != $b['hits'])
      return ($a['hits'] < $b['hits'] ? -1 : 1);
    return strcmp($a['full_path'], $b['full_path']);
  }

  if ($sort == "mtime") {
    if ($a['timestamp'] != $b['timestamp'])
      return ($a['timestamp'] < $b['timestamp'] ? -1 : 1);
    return strcmp($a['full_path'], $b['full_path']);
  }

  if ($sort == "filename") {
    return strcmp($a['full_path'], $b['full_path']);
  }

  return 0;
};

$_fn_sort_rev = function($a, $b) use ($_fn_sort) {
  return -$_fn_sort($a, $b);
};

$_fn_sort_final = ($sort_dir == 'asc' ? $_fn_sort : $_fn_sort_rev);
if ($sort != "")
  uasort($scripts, $_fn_sort_final);


?>
<!DOCTYPE html>
<html>
<head>
  <style type="text/css">
body {
  font-family: sans-serif;
}
h1 a {
  color: inherit;
  text-decoration: none;
}
p.name {
  position: absolute;
  top: 1em;
  right: 1em;
  margin: 0;
  padding: 0.5em;
  border: 1px solid #000;
  font-family: monospace;
}
.low-stats th {
  font-weight: normal;
  text-align: left;
  font-family: monospace;
}

table.scripts {
}
  table.scripts thead th {
    padding: 1ex;
  }
  table.scripts tbody th {
    font-weight: normal;
    text-align: left;
  }
  table.scripts tbody td.h {
    text-align: right;
    padding-right: 1ex;
  }
  table.scripts tbody td.m {
    text-align: right;
    padding-right: 1ex;
  }
  </style>
</head>
<body>

<h1><a href=".">ThGnet <?= $image_name; ?> v<?= $sys_version; ?></a></h1>
<?php if ($sys_name != ""): ?><p class="name"><?= $sys_name; ?></p><?php endif; ?>

<h2>Zend OPcache status page</h2>

<table class="low-stats">
  <tbody>
    <tr>
      <th>opcache_enabled</th>
      <td><?php echo ($status['opcache_enabled'] ? _green("true") : _red("false")); ?></td>
    </tr>
    <tr>
      <th>cache_full</th>
      <td><?php echo ($status['cache_full'] ? _red("true") : _green("false")); ?></td>
    </tr>
    <tr>
      <th>restart_pending</th>
      <td><?php echo ($status['restart_pending'] ? _red("true") : _green("false")); ?></td>
    </tr>
    <tr>
      <th>restart_in_progress</th>
      <td><?php echo ($status['restart_in_progress'] ? _red("true") : _green("false")); ?></td>
    </tr>
    <tr>
      <th>memory</th>
      <td><?php echo _fmt_mib($status['memory_usage']['used_memory']); ?>
        / <?php echo _fmt_mib($status['memory_usage']['total_memory']); ?>
         (<?php echo _fmt_perc(1 - $status['memory_usage']['free_percentage']); ?>)</td>
    </tr>
    <tr>
      <th>cached scripts</th>
      <td><?php echo $stats['num_cached_scripts']; ?></td>
    </tr>
    <tr>
      <th>cached keys</th>
      <td><?php echo _fmt_num($stats['num_cached_keys']); ?> /
          <?php echo _fmt_num($stats['max_cached_keys']); ?></td>
    </tr>
    <tr>
      <th>hits</th>
      <td><?php echo _fmt_num($stats['hits']); ?></td>
    </tr>
    <tr>
      <th>misses</th>
      <td><?php echo _fmt_num($stats['misses']); ?></td>
    </tr>
  </tbody>
</table>

<table class="scripts">
  <thead>
    <tr>
      <th><a href="<?= _sort_helper('filename', 'asc'); ?>">Filename</a></th>
      <th><a href="<?= _sort_helper('hits', 'desc'); ?>">Hits</a></th>
      <th><a href="<?= _sort_helper('memory', 'desc'); ?>">Memory</a></th>
      <th><a href="<?= _sort_helper('atime', 'desc'); ?>">Last access</a></th>
      <th><a href="<?= _sort_helper('mtime', 'desc'); ?>">Timestamp</a></th>
    </tr>
  </thead>
  <tbody>
<?php foreach ($scripts as $script): ?>
  <tr>
    <th><?= $script['full_path']; ?></th>
    <td class="h"><?= $script['hits']; ?></th>
    <td class="m"><?= _fmt_kib($script['memory_consumption']); ?>
    <td class="at"><?= _fmt_timestamp($script['last_used_timestamp']); ?>
    <td class="mt"><?= _fmt_timestamp($script['timestamp']); ?>
  </tr>
<?php endforeach; ?>
  </tbody>
</table>
