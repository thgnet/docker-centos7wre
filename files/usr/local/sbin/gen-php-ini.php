<?php

function err($message)
{
  fprintf(STDERR, "Error: %s\n", $message);
  exit(1);
}

$local_args = $argv;
$progname = array_shift($local_args);
$output_file = array_shift($local_args);

if ($output_file == "")
  err("Missing output file parameter");

$env_version = (string) getenv("RUNTIME_VERSION");
if ($env_version == "")
  err("Missing env 'RUNTIME_VERSION'");

$env_type = (string) getenv("RUNTIME_TYPE");
if ($env_type == "")
  err("Missing env 'RUNTIME_TYPE'");

$env_exts = (string) getenv("RUNTIME_EXTENSIONS");
$exts = ($env_exts != "" ? explode(" ", $env_exts) : array());

$lines = file(__DIR__ . "/gen-php-ini.tpl.ini");

$buffer = "";
$context = array();
foreach ($lines as $ln => $line) {
  $current_state = (count($context) > 0 ? $context[0]['state'] : true);

  if (substr($line, 0, 1) == "#") {
    if (preg_match('/^#\s*if ver (<=|<|>|>=) (.*)\n$/', $line, $regp)) {
      $_ver_oper = $regp[1];
      $_ver_target = $regp[2];
      switch ($_ver_target) {
      case '<=':
        $_value = strcmp($_ver_target, $env_version) <= 0;
        break;
      case '<':
        $_value = strcmp($_ver_target, $env_version) < 0;
        break;
      case '>':
        $_value = strcmp($_ver_target, $env_version) > 0;
        break;
      case '>=':
        $_value = strcmp($_ver_target, $env_version) >= 0;
        break;
      }
      $current_state = $current_state && $_new_state;
      array_unshift($context, array(
        'state' => $current_state));
    }
    elseif (preg_match('/^#\s*if type (production|development)\n$/', $line, $regp)) {
      $_type_name = $regp[1];
      $_new_state = ($_type_name == $env_type);
      $current_state = $current_state && $_new_state;
      array_unshift($context, array(
        'state' => $current_state));
    }
    elseif (preg_match('/^#\s*if ext (.*)\n$/', $line, $regp)) {
      $_ext_name = $regp[1];
      $_new_state = in_array($_ext_name, $exts);
      $current_state = $current_state && $_new_state;
      array_unshift($context, array(
        'state' => $current_state));
    }
    elseif (preg_match('/^#\s*endif\n$/', $line, $regp)) {
      array_shift($context);
    }
    elseif (!preg_match('/^#;.*\n$/', $line, $regp))
      err("Invalid directive at line " . ($ln + 1) . ": " . rtrim($line, "\n"));
  }
  else {
    if ($current_state) {
      $buffer .= $line;
    }
  }
}

$fd = fopen($output_file, "w");
fputs($fd, "; Dynamic php.ini generated with:\n");
fputs($fd, ";    VERSION = $env_version\n");
fputs($fd, ";    TYPE = $env_type\n");
fputs($fd, ";    EXTENSIONS = " . (count($exts) > 0 ? implode(" ", $exts) : "(none)") . "\n");
fputs($fd, "\n");
fputs($fd, trim(preg_replace('/\n\n+/', "\n\n", $buffer)) . "\n");
fclose($fd);
