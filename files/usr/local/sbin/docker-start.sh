#!/bin/bash

set -e

echo
echo " ******************************************************* "
echo " *                 PHP Web Environment                 * "
echo " ******************************************************* "
echo

case $RUNTIME_VERSION in
php53)
  my_version=php53
  php_extensions_dir="extensions/no-debug-non-zts-20090626/"
  php_binaries="php phar"
  ;;
php72)
  my_version=php72
  php_extensions_dir="20170718/"
  php_binaries="php phar"
  ;;
php80)
  my_version=php80
  php_extensions_dir="20200930/"
  php_binaries="php phar"
  ;;
php81)
  my_version=php81
  php_extensions_dir="20210902/"
  php_binaries="php phar"
  ;;
php82)
  my_version=php82
  php_extensions_dir="20220829/"
  php_binaries="php phar"
  ;;
php83)
  my_version=php83
  php_extensions_dir="20230831/"
  php_binaries="php phar"
  ;;
*)
  my_version=php72
  extensions_dir="20170718/"
  echo "[!] Please set environmental variable \"RUNTIME_VERSION\" to one of the following values:"
  echo "  - php53"
  echo "  - php72 [default]"
  echo "  - php74"
  echo "  - php80"
  echo "  - php81"
  echo "  - php82"
  echo "  - php83"
  echo
esac

for xbin in $php_binaries
do
  ln -s /usr/local/bin/$xbin /opt/php/$my_version/bin/$xbin
done

case $RUNTIME_TYPE in
production|product|prod)
  my_runtime=production
  ;;
development|devel|dev)
  my_runtime=development
  ;;
*)
  my_runtime=development
  echo "[!] Please set environmental variable \"RUNTIME_TYPE\" to one of the following values:"
  echo "  - development (or dev) [default]"
  echo "  - production (or prod)"
  echo
  ;;
esac

# Enable dynamic extension by modifying a copy of the original ini file
cp /opt/php/$my_version/etc/php.ini-$my_runtime /opt/php/$my_version/etc/php.ini-runtime
for xext in $RUNTIME_EXTENSIONS
do
  if [ "$xext" = "opcache" ]
  then
    echo "[+] Enabling dynamic extension Zend OPcache..."
    sed -i 's/;\(zend_extension=.*opcache.so\)$/\1/' /opt/php/$my_version/php.ini-runtime
  else
    echo "[!] Error: Unknown dynamic extension \"$xext\" for \"$my_version\""
  fi
  echo
done

# Always store a template file for the php ini
rm -f /srv/app/.htphp-dist.ini
cp /usr/local/lib/php.ini-runtime /srv/app/.htphp-dist.ini

# Determine effective php.ini
php_ini=/usr/local/lib/php.ini-runtime
if [ -e /srv/app/.htphp.ini ]
then
  echo "[!] Using app-specific PHP ini file (overrides dynamic extensions)"
  php_ini=/srv/app/.htphp.ini
  echo
else
  echo "[+] Using $my_runtime PHP ini file (you can override it with '.htphp.ini')"
  echo
fi
rm -f /usr/local/etc/php.ini
ln -s $php_ini /usr/local/etc/php.ini

# Support binding on a specific port for low-priviledged containers
if [ -n "$HTTPD_PORT" ]
then
  echo "[+] Using HTTP port $HTTPD_PORT (env HTTPD_PORT)"
  sed -i "s/^Listen .*$/Listen $HTTPD_PORT/" /etc/httpd/conf.d/x-listen.conf
else
  echo "[+] Using default HTTP port (env HTTPD_PORT)"
  sed -i "s/^Listen .*$/Listen 0.0.0.0:80/" /etc/httpd/conf.d/x-listen.conf
fi
echo

# Support for proxy trusted IP address
if [ -n "$HTTPD_PROXY_IP" ]
then
  echo "[+] Using trusted proxy IP \"$HTTPD_PROXY_IP\" (env HTTPD_PROXY_IP)"
  sed -i "s/^#\?RemoteIPTrustedProxy .*$/RemoteIPTrustedProxy \"$HTTPD_PROXY_IP\"/" /etc/httpd/conf.d/x-proxy-ip.conf
else
  echo "[+] No trusted proxy IP configured (env HTTPD_PROXY_IP)"
  sed -i "s/^#\?RemoteIPTrustedProxy .*$/#RemoteIPTrustedProxy \"\"/" /etc/httpd/conf.d/x-proxy-ip.conf
fi
echo

echo "[+] Checking environment..."
my_ipaddr=`hostname -i || :`
my_hostname=`hostname -f || :`
my_domain=`hostname -d || :`
echo "  ==> ADDRESS    : $my_ipaddr"
echo "  ==> HOSTNAME   : $my_hostname"
echo "  ==> DOMAIN      : $my_domain"
echo "  ==> RUNTIME    : $my_runtime"
echo

echo "[+] Checking required folders..."
# app docs files FIXME: chown?
mkdir -p /srv/app/htdocs
# data
mkdir -p /srv/data
chmod 1777 /srv/data
# sessions folder (public access)
mkdir -p /srv/data/sessions
chown webuser.webuser /srv/data/sessions
chmod 777 /srv/data/sessions
# apache logs (only root)
mkdir -p /srv/data/htlogs
#chown -R root.root /srv/data/htlogs
mkdir -p /srv/data/htlogs/system
chmod 700 /srv/data/htlogs/system
echo

##############################################################################
# Generation of ECFG data file
#
echo "[+] Checking environment configuration (ECFG)..."
echo "RUNTIME_TYPE=$my_runtime" > /srv/config/ecfg.ini
while IFS='=' read -r name value
do
  if [[ $name == 'ECFG_'* ]]
  then
    echo "  ==> $name"
    echo "$name=$value" >> /srv/config/ecfg.ini
    unset $name
  fi
done < <(env)
echo

#############################################################################
# Configuration of SMTP relay service (sendmail)
#
if [ -n "$SMTP_RELAY_HOST" ]
then
  my_smtp_relay="${SMTP_RELAY_HOST}"
  my_smtp_domain="${SMTP_RELAY_DOMAIN:-$my_domain}"
  echo "[+] Configuring SMTP relay service..."
  echo "  ==> SMTP RELAY  : $my_smtp_relay"
  echo "  ==> SMTP DOMAIN : $my_smtp_domain"
  cat > /etc/ssmtp/ssmtp.conf <<_SMTP_
root=postmaster
mailhub=$my_smtp_relay
hostname=$my_smtp_domain
_SMTP_
else
  echo "[!] Warning: You should set the SMTP_RELAY_HOST environment variable to enable sendmail"
  rm -f /etc/ssmtp/ssmtp.conf
fi
echo

##############################################################################
# Installation of initial user crontab
#
if [ "$RUNTIME_CRON" = "true" ]
then
  echo "[*] Enabling crontab support... (env RUNTIME_CRON=true)"
  sed -i -e 's/^autostart=false/autostart=true/' \
      /etc/supervisord.d/crond.ini
  if [ -f /srv/app/.htcron ]
  then
    echo "Installing user crontab file (.htcron)"
    crontab -u webuser /srv/app/.htcron
  else
    echo "No user crontab file (.htcron)"
  fi
else
  echo "[*] Not enabling crontab support (env RUNTIME_CRON=true)"
fi
echo

##############################################################################
# Setup of the shell environment
#
if [ "$RUNTIME_SSH" = "true" ]
then
  echo "[*] Enabling ssh support... (env RUNTIME_SSH=true)"
  mkdir -p /home/sshd
  chown root.root /home/sshd
  chmod 755 /home/sshd
  if [ ! -e /home/sshd/ssh_host_rsa_key ]
  then
    ssh-keygen -q -t rsa -f /home/sshd/ssh_host_rsa_key -C '' -N ''
    chgrp ssh_keys /home/sshd/ssh_host_rsa_key
    chmod 640 /home/sshd/ssh_host_rsa_key
  fi
  if [ ! -e /home/sshd/ssh_host_ecdsa_key ]
  then
    ssh-keygen -q -t ecdsa -f /home/sshd/ssh_host_ecdsa_key -C '' -N ''
    chgrp ssh_keys /home/sshd/ssh_host_ecdsa_key
    chmod 640 /home/sshd/ssh_host_ecdsa_key
  fi
  if [ ! -e /home/sshd/ssh_host_ed25519_key ]
  then
    ssh-keygen -q -t ed25519 -f /home/sshd/ssh_host_ed25519_key -C '' -N ''
    chgrp ssh_keys /home/sshd/ssh_host_ed25519_key
    chmod 640 /home/sshd/ssh_host_ed25519_key
  fi
  if [ ! -e /home/sshd/hosts.deny ]
  then
    echo 'sshd : ALL' > /home/sshd/hosts.deny
    chmod 600 /home/sshd/hosts.deny
  fi
  if [ ! -e /home/sshd/hosts.allow ]
  then
    echo 'sshd : LOCAL' > /home/sshd/hosts.allow
    chmod 600 /home/sshd/hosts.allow
  fi
  if [ ! -e /home/sshd/authorized_keys ]
  then
    touch /home/sshd/authorized_keys
    chmod 644 /home/sshd/authorized_keys
  fi
  sed -i -e 's/^autostart=false/autostart=true/' \
      /etc/supervisord.d/sshd.ini

  if [ -n "$SSHD_USER_ID" -a -n "$SSHD_USER_NAME" ]
  then
    # Create user passwd entry
    groupadd -g $SSHD_USER_ID $SSHD_USER_NAME || :
    useradd -u $SSHD_USER_ID -g $SSHD_USER_ID -G webuser \
        -d /home/$SSHD_USER_NAME -M $SSHD_USER_NAME || :
    rm -f /etc/*-
    # Create home directory
    if [ ! -e /home/$SSHD_USER_NAME ]
    then
      echo "Creating HOME directory for '$SSHD_USER_NAME'..."
      cp -pr /etc/skel /home/$SSHD_USER_NAME
      mkdir /home/$SSHD_USER_NAME/.ssh
      touch /home/$SSHD_USER_NAME/.ssh/authorized_keys
      chown -R $SSHD_USER_NAME:$SSHD_USER_NAME /home/$SSHD_USER_NAME
      chmod 700 /home/$SSHD_USER_NAME
      chmod 700 /home/$SSHD_USER_NAME/.ssh
      chmod 600 /home/$SSHD_USER_NAME/.ssh/authorized_keys
    fi
  else
    echo "[!] Error: You must set the SSHD_USER_ID and SSHD_USER_NAME environment variables"
    exit 1
  fi
else
  echo "[*] Not enabling ssh support (env RUNTIME_SSH=true)"
fi
echo

# FIXME: this was needed because in some situations apache will refuse to start
echo "[+] Cleaning up runtime httpd files..."
find /run/httpd -type f -print -delete
echo

##############################################################################
# Startup
#
echo "[+] Starting supervisor..."
exec /usr/bin/supervisord -n -c /etc/supervisord.ini
