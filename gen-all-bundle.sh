#!/bin/bash

set -eu

mkdir -p tmp.bundles

set -x

cd tmp.bundles

productsdir="../builder/builder-products"
outfile="php-all-build-x86_64.tar.gz"

do_unpack() {
  echo "== Unpacking single version bundles..."
  rm -rf php*
  for mfile in $(cd $productsdir; echo *.tar.gz.md5)
  do
    local bfile=${mfile%*.md5}
    phptag=${bfile%%-*}
    echo "Unpacking '$bfile' as '$phptag'..."
    (cd $productsdir; md5sum -c $mfile)
    mkdir $phptag
    tar -C $phptag -zxf $productsdir/$bfile
  done
}

do_shrink() {
  echo "== Cleaning up unwanted files..."
  rm -rf php*/lib/php/build
  rm -rf php*/include
  rm -rf php*/man
  rm -rf php*/lib/php/test
  rm -rf php*/lib/php/.registry
  rm -rf php*/lib/php/.lock
  rm -rf php*/lib/php/doc
}

do_symlinks() {
  echo "== Creating symlinks for configuration files..."
  for phptag in php*
  do
    mkdir -p $phptag/etc
    ln -s /usr/local/lib/php.ini $phptag/etc/php.ini
    touch -r $phptag/bin/php -h $phptag/etc/php.ini
  done
}

do_stripping() {
  echo "== Stripping executables..."
  for phptag in php*
  do
    strip -p $phptag/bin/php
    find . -name "*.so" -exec strip -p {} \;
  done
}

do_repack() {
  echo "== Repacking '$outfile'..."
  for phptag in php*
  do
    echo "Fixing timestamps for '$phptag'..."
    find $phptag -depth -type d -exec touch -r $phptag/bin/phpize {} \;
  done
  find * | LC_ALL=C sort > ../$outfile.tmp.list
  (
    tar --numeric-owner --owner=0 --group=0 \
      --mode=a-s \
      --no-recursion \
      --files-from=../$outfile.tmp.list -c
  ) | gzip -9nc > ../$outfile.tmp
  mv ../$outfile.tmp ../$outfile
  rm ../$outfile.tmp.list
}

do_reports() {
  echo "== Generating report files..."
  tar ztf ../$outfile > ../$outfile.files
  TZ=UTC tar ztvf ../$outfile > ../$outfile.manifest
  (cd ..; ls -lh $outfile*)
}

do_all() {
  local target="${1:-}"
  do_unpack
  [ "$target" = "unpack" ] && return 0
  do_shrink
  [ "$target" = "shrink" ] && return 0
  do_symlinks
  [ "$target" = "symlinks" ] && return 0
  do_stripping
  [ "$target" = "strip" ] && return 0
  do_repack
  [ "$target" = "repack" ] && return 0
  do_reports
  [ "$target" = "reports" ] && return 0
  return 0
}

do_"${1:-all}" "${2:-}"

# cd ..
# rm -rf tmp.bundles

echo "Operation completed"
