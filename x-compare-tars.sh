#!/bin/bash

set -eu

phptag="${1:-}"
if [ -z "$phptag" ]
then
  echo "Usage: $0 <phptag>" >&2
  exit 1
fi

xdiff_file_md5="builder/builder-products/${phptag}-build-x86_64.tar.gz.md5"
if [ ! -e "$xdiff_file_md5" ]
then
  echo "Error: Cannot locate file \"$xdiff_file_md5\"" >&2
  exit 1
fi

xdiff_md5_1=$(git show :$xdiff_file_md5 | awk '{ print $1 }')
xdiff_label_1="$phptag (git)"
xdiff_md5_2=$(cat "$xdiff_file_md5" | awk '{ print $1 }')
xdiff_label_2="$phptag (wc)"
echo ""
echo "Comparing files:"
echo "    HEAD: $xdiff_md5_1  $xdiff_label_1"
echo "    WORK: $xdiff_md5_2  $xdiff_label_2"
echo ""

if [ "$xdiff_md5_1" = "$xdiff_md5_2" ]
then
  echo "Files are identical"
  exit 0
fi

xdiff_file_1="builder/builder-products/archive/${phptag}-build-x86_64.$xdiff_md5_1.tar.gz"
if [ ! -e "$xdiff_file_1" ]
then
  echo "Error: Cannot locate file \"$xdiff_file_1\"" >&2
  exit 1
fi

xdiff_file_2="builder/builder-products/archive/${phptag}-build-x86_64.$xdiff_md5_2.tar.gz"
if [ ! -e "$xdiff_file_2" ]
then
  echo "Error: Cannot locate file \"$xdiff_file_2\"" >&2
  exit 1
fi

rm -rf xdiff_1 xdiff_2
mkdir -p xdiff_1 xdiff_2
cat "$xdiff_file_1" | tar -C xdiff_1 -zx
cat "$xdiff_file_2" | tar -C xdiff_2 -zx

fixup_php()
{
  # fixup pear reg
  for ii in $(find $1 -name "*.reg")
  do
    cat $ii | php -r 'var_export(unserialize(stream_get_contents(STDIN)));' > $ii.var
    rm $ii
  done

  # fixup pear conf
  for ii in $(find $1 -name "pear.conf")
  do
    tail -n +2 $ii | php -r 'var_export(unserialize(stream_get_contents(STDIN)));' > $ii.var
    rm $ii
  done

  # fixup phar
  for ii in $(find $1 -name "*.phar")
  do
    ./builder/builder-files/phardump.php "$ii" > "$ii.dump"
    hexdump -C $ii > $ii.hex
  done

  # add strings to binary files to help investigate
  for ii in $(find $1 -name "*.so") $1/bin/php
  do
    strings -6 $ii > $ii.strings
    #hexdump -C $ii > $ii.hex
  done
}

echo "Applying PHP bundle fixups"
fixup_php xdiff_1
fixup_php xdiff_2

diff -ur --no-dereference xdiff_1 xdiff_2
