#!/bin/bash

set -eu
cd -- "$(dirname -- "${BASH_SOURCE[0]}")"

# resolve the version and builder tag (needed to select the image)
image_version="$(sed -n -e 's/^#:VERSION://p' Dockerfile.tpl.sh)"
image_release="$(sed -n -e 's/^#:RELEASE://p' Dockerfile.tpl.sh)"
builder_tag="$(sed -n -e "s/@@VERSION@@/$image_version/; s/@@RELEASE@@/$image_release/; s/^\#:BUILDER_TAG://p" Dockerfile.tpl.sh | head -n 1)"

echo
echo "  Version: $image_version"
echo "  Release: $image_release"
echo "    Image: $builder_tag"
echo

# build builder
make builder

# run builder
docker run \
  --name centos7wre_builder_dev \
  --hostname centos7wre-builder-$(cat /dev/urandom | tr -cd 'a-z0-9' | head -c 8).svcs.thgnet.it \
  -v `pwd`/builder/builder-runtime:/srv/build/runtime \
  -v `pwd`/builder/builder-products:/srv/build/products \
  -v `pwd`/builder/builder-files:/opt/builder-files \
  -it --rm \
  $builder_tag \
  /bin/bash
