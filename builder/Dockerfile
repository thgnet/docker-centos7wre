FROM thgnet/centos7:20240109

LABEL \
  org.label-schema.build-date="2024-03-15T11:00:00Z" \
  org.label-schema.name="PHP Web Environment" \
  org.label-schema.vendor="ThGnet" \
  org.label-schema.version="2.2" \
  org.opencontainers.image.created="2024-03-15 11:00:00+00:00" \
  org.opencontainers.image.title="PHP Web Environment" \
  org.opencontainers.image.vendor="ThGnet"

RUN set -eux; \
  sed -i '/^override_install_langs=/d' /etc/yum.conf; \
  yum -y reinstall glibc-common; \
  yum -y install \
      file bzip2 openssl httpd \
      libxslt libpng libicu libjpeg-turbo freetype libmcrypt libzip oniguruma \
      mariadb-libs \
      gmp libwebp libXpm readline libmemcached \
      libedit aspell libtool-ltdl; \
  groupadd -g 5000 webuser; \
  useradd -u 5000 -g 5000 -d /srv/app -M -c "Web user" webuser; \
  echo "2.2" > /opt/VERSION; \
  yum -y install \
      cronie ssmtp inotify-tools; \
  yum -y install \
      openssh-server mariadb-server sudo \
      nano vim wget telnet screen dos2unix patch diffstat colordiff \
      wget bzip2 unzip bind-utils whois nmap nmap-ncat zip \
      git subversion mercurial mariadb \
      net-tools psmisc bash-completion inotify-tools gettext; \
  ln -s /usr/bin/nano /usr/local/bin/pico; \
  sed -i \
      -e 's|^\(#\?HostKey\) /etc/ssh/|\1 /home/sshd/|' \
      -e 's|^\(AuthorizedKeysFile\)\s\+.*|\1 .ssh/authorized_keys /home/sshd/authorized_keys|' \
      -e 's|^\(PasswordAuthentication\) .*|\1 no|' \
      -e 's|^\(GSSAPIAuthentication\) .*|\1 no|' \
      /etc/ssh/sshd_config; \
  rm -f /etc/hosts.allow /etc/hosts.deny; \
  ln -s /home/sshd/hosts.allow /etc/hosts.allow; \
  ln -s /home/sshd/hosts.deny /etc/hosts.deny; \
  yum -y install \
      supervisor rsyslog; \
  mv /etc/supervisord.conf /etc/supervisord.ini; \
  ln -s supervisord.ini /etc/supervisord.conf; \
  mkdir -p /srv/{app,config,data}; \
  touch /srv/config/ecfg.ini; \
  pwck -s; rm -f /etc/passwd- /etc/shadow-; \
  grpck -s; rm -f /etc/group- /etc/gshadow-; \
  yum clean all;

RUN set -eux; \
  yum -y install \
    gcc gcc-c++ make re2c autoconf bison \
    aspell-devel \
    bison-devel \
    bzip2-devel \
    freetype-devel \
    gmp-devel \
    httpd-devel \
    libXpm-devel \
    libcurl-devel \
    libedit-devel \
    libicu-devel \
    libjpeg-turbo-devel \
    libmcrypt-devel \
    libmemcached-devel \
    libpng-devel \
    libtool-ltdl-devel \
    libwebp-devel \
    libxml2-devel \
    libxslt-devel \
    libzip-devel \
    mariadb-devel \
    ncurses-devel \
    oniguruma-devel \
    openssl-devel \
    readline-devel;

ADD builder-files /opt/builder-files

RUN set -eux; \
  mkdir -p /srv/build/products; \
  mkdir -p /srv/build/runtime; \
  mkdir -p /opt/php; \
  cp -a /opt/builder-files/build-all-php.sh /usr/local/bin/build-all-php; \
  echo 'export PATH=$PATH:/opt/builder-files' >> /etc/bashrc

VOLUME \
  /srv/build/products

WORKDIR /srv/build/runtime

CMD [ "/opt/build-all-php.sh" ]
