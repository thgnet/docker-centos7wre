#!/bin/bash
#
# docker-centos7wre/builder/builder-files/build-php82.inc.sh
#

php82.unpack.fixup() {
  :
}

php82.configure.args() {
  php_configure_args=(
    --prefix=/opt/php/$phptag
    --with-apxs2=/usr/bin/apxs
    --disable-phpdbg
    --disable-cgi
    --with-layout=GNU
    --disable-short-tags
    --with-openssl
    --with-zlib
    --with-bz2=shared
    --enable-calendar
    --with-curl
    --enable-dba=shared
    --enable-exif
    --enable-gd                    # ext/gd
    --with-webp                    # ext/gd/webp
    --with-jpeg                    # ext/gd/jpeg
    --with-xpm                     # ext/gd/xpm
    --with-freetype                # ext/gd/freetype
    --with-gettext                 # ext/gettext
    --enable-intl                  # ext/intl
    --enable-mbstring              # ext/mbstring
    --with-mysqli                  # ext/mysqli
    --without-sqlite3              # ext/sqlite
    --without-pdo-sqlite
    --enable-pcntl
    --with-readline
    --enable-soap
    --enable-sockets
    --without-zip                  # ext/zip
    --with-iconv                   # ext/iconv
  )
  :
}

php82.configure.fixup() {
  # Ugly hack to skip apache config file modification
  sed -i 's/i -a -n php/i -n php/' Makefile
  :
}

php82.build.fixup() {
  :
}

php82.install.fixup() {
  mv /usr/lib64/httpd/modules/libphp.so /opt/php/$phptag/lib/mod_php.so
  if [ -n "$SOURCE_DATE_EPOCH" ]
  then
    /opt/php/$phptag/bin/php $filesdir/pharfix.php /opt/php/$phptag/bin/phar.phar $SOURCE_DATE_EPOCH
  fi
  :
}

php82.pecl.install() {
  pecl_install apcu-5.1.21
  pecl_install memcached-3.2.0
  # pecl_install xdebug-3.1.5
  :
}

php82.pear.install() {
  # pear install "Mail"
  # pear install "Mail_Mime"
  # pear install "Auth_SASL"
  # pear install "Net_SMTP"
  :
}

php82.package.fixup() {
  mv share/man man
  rmdir share
  :
}
