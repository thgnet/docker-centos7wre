#!/bin/bash
#
# docker-centos7wre/builder/builder-files/xbuild-php.sh
#
# Compiles PHP and generates a binary install package
#

set -eu
debug="${X_BUILDER_DEBUG:-}"

out_section_start() {
  echo -en "\e[0Ksection_start:`date +%s`:$1\r\e[0K\e[43;37;1m$2\e[m\n"
  if [ -n "$debug" ]
  then
    set -x
  fi
}
out_section_end() {
  echo -en "\e[0Ksection_end:`date +%s`:$1\r\e[0K\n"
  set +x
}
out_normal() {
  echo -en "\e[33m[+++] $1\e[m\n\n"
}

workdir="/srv/build/runtime"
filesdir="/opt/builder-files"

if [ "${1:-}" = "-debug" ]
then
  debug="1"
  shift
fi

if [ -z "${1:-}" ]
then
  echo "Usage: $0 <phptag> <target> [params...]" >&2
  exit 1
fi
phptag="$1"
shift

if [ ! -f "$filesdir/xbuild-$phptag.inc.sh" ]
then
  echo "Error: Invalid tag \"$phptag\"" >&2
  exit 1
fi
. $filesdir/xbuild-$phptag.inc.sh

phpversion="$(echo $filesdir/php/php-${phptag:3:1}.${phptag:4}.*.tar.bz2 | sed -n 's|^.*/php-\(.*\)\.tar\.bz2$|\1|p')"

srcname="php-$phpversion"
srcdir="$workdir/$srcname"
pecldir="$workdir/$srcname/pecl"
installdir="/opt/php/$phptag"

outdir="/srv/build/products"
outfile="$phptag-build-x86_64.tar.gz"

if [ -n "${X_BUILDER_DUMMY:-}" ]
then
  echo "Creating dummy products for testing purposes"
  echo "dummy file $phptag at `date`" > $outdir/dummytest-$phptag.xfile
  exit 0
fi

export PATH="$PATH:$installdir/bin"
export SOURCE_DATE_EPOCH=$(date --utc --date="$(cat $filesdir/php/$srcname.tar.bz2.stamp)" +%s)
export PHP_UNAME="$(uname) buildhostname 3.10.0-1160.71.1.el7.x86_64 #1 SMP Tue Jun 28 15:37:28 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux"

cd $workdir

if [ "${1:-}" = "-prep" ]
then
  if [ -e $srcname-orig ]
  then
    out_normal "Original source already unpacked"
  else
    out_normal "Unpacking original source..."
    rm -rf $srcname-orig
    mkdir orig.tmp
    tar -C orig.tmp -jxf $filesdir/php/$srcname.tar.bz2
    mv orig.tmp/$srcname $srcname-orig
    rmdir orig.tmp
    out_normal "Done."
  fi
  exit
fi

if [ "${1:-}" = "-patch" ]
then
  out_normal "Updating source patch file..."
  diff -U 5 -r $srcname-orig $srcname | \
    sed -e '/^diff /d; s/^\([+-]\+ \)php-[0-9A-Za-z.-]\+\/\(.*\)\t[0-9:.+ -]\+/\1\2/' > $srcname.patch
  if [ -s $srcname.patch ]
  then
    if cmp -s $srcname.patch $filesdir/php/$srcname.patch
    then
      echo "Already up to date: $filesdir/php/$srcname.patch"
    else
      cat $srcname.patch > $filesdir/php/$srcname.patch
      echo "Patch file updated: $filesdir/php/$srcname.patch"
    fi
  else
    echo "No patch file necessary (empty)"
    rm -f $filesdir/php/$srcname.patch
  fi
  rm -f $srcname.patch
  out_normal "Done."
  exit
fi

if [ "${1:-}" = "-repatch" ]
then
  xbuild-php.sh $phptag -prep
  xbuild-php.sh $phptag unpack
  xbuild-php.sh $phptag -patch
  exit
fi


##############################################################################

do_unpack() {
  out_section_start 'do_unpack' "Unpacking source files"
  out_normal "Unpacking source file '$filesdir/php/$srcname.tar.bz2' into '$workdir/$srcname'..."
  rm -rf $srcname
  if [ -n "$debug" ]
  then
    tar -jxvf $filesdir/php/$srcname.tar.bz2
  else
    tar -jxf $filesdir/php/$srcname.tar.bz2
  fi
  pushd $srcname
  ${phptag}.unpack.fixup
  popd
  if [ -e "$filesdir/php/$srcname.patch" ]
  then
    echo "Applying local build patch '$srcname.patch'"
    (cd $srcdir; patch -p0 --no-backup-if-mismatch < $filesdir/php/$srcname.patch)
  fi
  out_section_end 'do_unpack'
}

##############################################################################

do_configure() {
  out_section_start 'do_configure' "Running configure script"
  pushd $srcdir
  out_normal "Starting configure script inside '$srcname'..."
  $phptag.configure.args
  if [ -n "$debug" ]
  then
    ./configure "${php_configure_args[@]}"
  else
    ./configure "${php_configure_args[@]}" --quiet
  fi
  $phptag.configure.fixup
  popd
  out_section_end 'do_configure'
}

##############################################################################

do_build() {
  out_section_start 'do_build' "Building main source"
  pushd $srcdir
  cpus=`grep -c ^processor /proc/cpuinfo`
  out_normal "Starting compilation with $cpus processor/s..."
  if [ -n "$debug" ]
  then
    make -j $cpus
  else
    make -j $cpus -s
  fi
  $phptag.build.fixup
  popd
  out_section_end 'do_build'
}

##############################################################################

do_install() {
  out_section_start 'do_install' "Installing main source"
  pushd $srcdir
  out_normal "Installing in '$installdir'..."
  rm -rf $installdir
  make install
  $phptag.install.fixup
  popd
  out_section_end 'do_install'
}

##############################################################################

pecl_install() {
  local pecl_pkg="${1%-*}"
  local pecl_ver="${1##*-}"
  mkdir -p $pecldir
  pushd $pecldir
  out_normal "Adding PECL '$pecl_pkg' version $pecl_ver..."
  tar -zxf $filesdir/pecl/$pecl_pkg-$pecl_ver.tgz
  if [ -e $filesdir/pecl/$pecl_pkg-$pecl_ver.patch ]
  then
    echo "Applying local build patch..."
    patch -p0 < $filesdir/pecl/$pecl_pkg-$pecl_ver.patch
  fi
  cd $pecl_pkg-$pecl_ver
  if [ -n "$debug" ]
  then
    phpize
    ./configure
    make
    make install
  else
    phpize
    ./configure --quiet
    make -s
    make install
  fi
}

do_pecl() {
  out_section_start 'do_pecl' "Installing PECL modules"
  $phptag.pecl.install
  out_section_end 'do_pecl'
}

##############################################################################

pear_install() {
  local pear_pkg="$1"
  out_normal "Installing PEAR module '$pear_pkg'..."
  pear install $pear_pkg
}

do_pear() {
  out_section_start 'do_pear' "Installing PEAR library"
  $phptag.pear.install
  out_section_end 'do_pear'
}

##############################################################################

do_package() {
  out_section_start 'do_package' "Packaging build products"
  out_normal "Packaging build product as '$outdir/$outfile'..."
  local tmp_dir=/tmp
  local tmp_pkgdir=$tmp_dir/pkg-$phptag
  local tmp_outfile=$tmp_dir/$outfile
  rm -rf $tmp_outfile $tmp_outfile.manifest $tmp_pkgdir
  cp -a $installdir $tmp_pkgdir
  echo "Using temporary directory '$tmp_pkgdir'"
  (
    cd $tmp_pkgdir
    $phptag.package.fixup
    find * | LC_ALL=C sort > $tmp_outfile.list
  )
  (
    cd $tmp_pkgdir
    tar --numeric-owner --owner=0 --group=0 \
      --mode=a-s --mtime="@$SOURCE_DATE_EPOCH" \
      --no-recursion \
      --files-from=$tmp_outfile.list -c
  ) | gzip -9nc > $tmp_outfile
  # Creates files and manifest
  tar -ztf $tmp_outfile > $tmp_outfile.files
  TZ=UTC tar -ztvf $tmp_outfile > $tmp_outfile.manifest
  # Checksum the bundle file
  (cd $tmp_dir; md5sum $outfile) > $tmp_outfile.md5
  # Create an archived copy of the bundle file based on the md5
  local hashfile="${outfile%.tar.gz}.$(cat $tmp_outfile.md5 | awk '{ print $1 }').tar.gz"
  mkdir -p $outdir/archive
  cp -a $tmp_outfile $outdir/archive/$hashfile
  # Move the relevant files to the products directory
  mv $tmp_outfile $tmp_outfile.md5 $tmp_outfile.files $tmp_outfile.manifest $outdir
  if [ -z "$debug" ]
  then
    rm -rf $tmp_pkgdir $tmp_outfile.list
  fi
  out_section_end 'do_package'
}

##############################################################################

do_clean() {
  out_section_start 'do_clean' "Cleaning up generated files"
  out_normal "Deleting source and installed data..."
  rm -rf $srcdir $installdir
  out_section_end 'do_clean'
}

##############################################################################

do_all() {
  local target="${1:-}"
  do_unpack
  [ "$target" = "unpack" ] && return 0
  do_configure
  [ "$target" = "configure" ] && return 0
  do_build
  [ "$target" = "build" ] && return 0
  do_install
  [ "$target" = "install" ] && return 0
  do_pecl
  [ "$target" = "pecl" ] && return 0
  do_pear
  [ "$target" = "pear" ] && return 0
  do_package
  [ "$target" = "package" -o -n "$debug" ] && return 0
  do_clean
  [ "$target" = "clean" ] && return 0
  return 0
}

##############################################################################

do_help() {
  echo "Usage: $0 <target>" >&2
  echo "Targets:" >&2
  echo " - unpack" >&2
  echo " - configure" >&2
  echo " - build" >&2
  echo " - install" >&2
  echo " - pecl" >&2
  echo " - pear" >&2
  echo " - package" >&2
  echo " - clean" >&2
  echo " - all" >&2
}

##############################################################################

case "${1-help}" in
  "help")
    do_help
    ;;
  "all")
    do_all "${2-}"
    ;;
  *)
    for action in $@
    do
      do_$action
    done
    ;;
esac

echo "Execution terminated successfully"

exit 0
