#!/bin/bash
#
# docker-centos7wre/builder/builder-files/build-php72.inc.sh
#

php72.unpack.fixup() {
  :
}

php72.configure.args() {
  php_configure_args=(
    --prefix=/opt/php/$phptag
    --with-apxs2=/usr/bin/apxs
    --disable-phpdbg
    --disable-cgi
    --with-layout=GNU
    --disable-short-tags
    --with-openssl
    --with-zlib
    --with-bz2=shared
    --enable-calendar
    --with-curl
    --enable-dba=shared
    --enable-exif
    --with-gd                      # ext/gd
    --with-webp-dir
    --with-jpeg-dir
    --with-png-dir
    --with-xpm-dir
    --with-freetype-dir
    --with-gettext
    --enable-intl
    --enable-mbstring
    --with-mysqli
    --enable-pcntl
    --with-readline
    --enable-soap
    --enable-sockets
    --enable-zip
    --disable-maintainer-zts
    --with-iconv
  )
  :
}

php72.configure.fixup() {
  # Ugly hack to skip apache config file modification
  sed -i 's/i -a -n php/i -n php/' Makefile
  :
}

php72.build.fixup() {
  :
}

php72.install.fixup() {
  mv /usr/lib64/httpd/modules/libphp7.so /opt/php/$phptag/lib/mod_php.so
  if [ -n "$SOURCE_DATE_EPOCH" ]
  then
    /opt/php/$phptag/bin/php $filesdir/pharfix.php /opt/php/$phptag/bin/phar.phar $SOURCE_DATE_EPOCH
  fi
  :
}

php72.pecl.install() {
  pecl_install apcu-5.1.21
  pecl_install memcached-3.2.0
  pecl_install inotify-3.0.0
  pecl_install xdebug-3.1.5
  :
}

php72.pear.install() {
  pear install "Mail"
  pear install "Mail_Mime"
  pear install "Auth_SASL"
  pear install "Net_SMTP"
  :
}

php72.package.fixup() {
  mv share/man man
  mv share/pear lib/php
  rmdir share
  :
}
