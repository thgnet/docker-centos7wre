#!/usr/bin/env php
<?php
/**
 * ...
 */

function read_binary($data, &$offset, $length)
{
  /* check that pos + length is not more than total */
  if (($offset + $length) > strlen($data))
    throw new \Exception("Out of bound: " .
        "Cannot read $length " . ($length == 1 ? "byte" : "bytes") . " " .
        "at position " . $offset . " (total " . strlen($data) . ")");

  $retval = substr($data, $offset, $length);
  $offset += $length;

  return $retval;
}

function read_uint16le($data, &$offset)
{
  /** @var array{1: int<0, 65535>} */
  $vals = unpack("v", read_binary($data, $offset, 2));

  return $vals[1];
}

function read_uint32le($data, &$offset)
{
  /** @var array{1: int<0, max>} */
  $vals = unpack("V", read_binary($data, $offset, 4));

  return $vals[1];
}

function phar_decode_manifest($data)
{
  $offset = 0;

  /* extract the number of entries */
  $entries = read_uint32le($data, $offset);

  /* extract the version */
  $version = read_uint16le($data, $offset);

  /* extract the global flags */
  $flags = read_uint32le($data, $offset);

  /* extract the alias */
  $_alias_length = read_uint32le($data, $offset);
  $alias = read_binary($data, $offset, $_alias_length);

  /* extract the metadata */
  $_metadata_length = read_uint32le($data, $offset);
  if ($_metadata_length) {
    $has_metadata = true;
    $metadata = unserialize($reader->read($_metadata_length));
  }
  else {
    $has_metadata = false;
    $metadata = null;
  }

  $index = array();

  $blob_offset = 0;
  for ($i = 0; $i < $entries; $i++) {
    $entry = array();

    /* extract the filename */
    $_filename_length = read_uint32le($data, $offset);
    $entry['filename'] = read_binary($data, $offset, $_filename_length);

    /* other file data */
    $entry['uncompressed_size'] = read_uint32le($data, $offset);
    $entry['timestamp'] = read_uint32le($data, $offset);
    $entry['compressed_size'] = read_uint32le($data, $offset);
    $entry['crc32'] = read_uint32le($data, $offset);
    $entry['flags'] = read_uint32le($data, $offset);

    /* file metadata */
    $_metadata_length = read_uint32le($data, $offset);
    if ($_metadata_length) {
      $entry['has_metadata'] = true;
      $entry['metadata'] = unserialize(read_binary($data, $offset, $_metadata_length));
    }
    else {
      $entry['has_metadata'] = false;
      $entry['metadata'] = null;
    }

    /* patch the calculated offset */
    $entry['_offset'] = $blob_offset;
    $blob_offset += $entry['compressed_size'];

    $index[] = $entry;
  }

  $retval = array();
  $retval['version'] = $version;
  $retval['flags'] = $flags;
  $retval['alias'] = $alias;
  $retval['has_metadata'] = $has_metadata;
  $retval['metadata'] = $metadata;
  $retval['index'] = $index;

  return $retval;
}

function phar_inspect_file($pharfile)
{
  /* check file size and open archive */
  $phar_size = @filesize($pharfile);
  if (!$phar_size || !($fd = @fopen($pharfile, "rb")))
    throw new \Exception("Cannot open phar file \"$pharfile\"");

  /* read the stub file */
  $stub_file = "";
  while (($_stub_line = fgets($fd)) !== false) {
    /* FIXME: bogus way, but the other one simply doesn't work */
    // FIXME: (2) nooooo... dev'essere una direttiva vera e propria. ^|;?
    // if (strpos($_stub_line, "__HALT_COMPILER") !== false)
      // break;
    if (substr($_stub_line, 0, 15) == "__HALT_COMPILER")
      break;
    $stub_file .= $_stub_line;
  }

  $manifest_offset = ftell($fd);

  /* read the manifest length */
  list(, $manifest_length) = unpack("V", fread($fd, 4));

  /* acquire the manifest */
  $manifest_data = "";
  while ($manifest_length > 0) {
    $manifest_data .= fread($fd, min($manifest_length, 8192));
    $manifest_length -= 8192;
  }
  if (strlen($manifest_data) < $manifest_length)
    throw new \Exception(
      "Manifest expected length was " . $manifest_length . " bytes " .
      "but only " . strlen($manifest_data) . " bytes available");

  $phardata = phar_decode_manifest($manifest_data);

  return $phardata;
}

$local_args = $argv;
$progname = array_shift($local_args);

$file = (string) array_shift($local_args);
if ($file == "") {
  fprintf(STDERR, "Usage: %s <file.phar>\n", $progname);
  exit(1);
}

$phardata = phar_inspect_file($file);

printf("\n");
printf("   Version: %s\n", $phardata['version']);
printf("     Flags: 0x%08x\n", $phardata['flags']);
printf("     Alias: %s\n", $phardata['alias']);
if ($phardata['has_metadata']) {
  printf("  Metadata: %s\n", json_encode($phardata['metadata']));
}
else {
  printf("  Metadata: (none)\n");
}
printf("\n");
printf("\n");
printf(" Size (C)    Size (U)        Timestamp        CRC32    Flags    Perms    Offset      Filename\n");
printf("----------  ----------  -------------------  --------  -----  ---------  ------  -------------------------------\n");
//      11'423'222  42'232'112  2010-22-22 21:22:59  859242d8  001b6  rwxrwxrwx  322004  path/to/file.php
foreach ($phardata['index'] as $entry) {
  printf("%10s  %10s  %s  %08x  %05x  %s  %06x  %s\n",
      number_format($entry['compressed_size'], 0, ".", "'"),
      number_format($entry['uncompressed_size'], 0, ".", "'"),
      date("Y-m-d H:i:s", $entry['timestamp']),
      $entry['crc32'],
      $entry['flags'],
        ($entry['flags'] & 0b100000000 ? "r" : "-") .
        ($entry['flags'] & 0b010000000 ? "w" : "-") .
        ($entry['flags'] & 0b001000000 ? "x" : "-") .
        ($entry['flags'] & 0b000100000 ? "r" : "-") .
        ($entry['flags'] & 0b000010000 ? "w" : "-") .
        ($entry['flags'] & 0b000001000 ? "x" : "-") .
        ($entry['flags'] & 0b000000100 ? "r" : "-") .
        ($entry['flags'] & 0b000000010 ? "w" : "-") .
        ($entry['flags'] & 0b000000001 ? "x" : "-"),
      $entry['_offset'],
      $entry['filename'] . ($entry['has_metadata'] ?
          " -- meta: " . json_encode($entry['metadata'], JSON_UNESCAPED_SLASHES) : ""));
}
printf("\n");
printf("Total entries: %d\n", count($phardata['index']));
printf("\n");
