#!/usr/bin/env php
<?php
/**
 * ...
 */

$opt_verbose = false;

function read_binary($data, &$offset, $length)
{
  /* check that pos + length is not more than total */
  if (($offset + $length) > strlen($data))
    throw new \Exception("Out of bound: " .
        "Cannot read $length " . ($length == 1 ? "byte" : "bytes") . " " .
        "at position " . $offset . " (total " . strlen($data) . ")");

  $retval = substr($data, $offset, $length);
  $offset += $length;

  return $retval;
}

function read_uint16le($data, &$offset)
{
  /** @var array{1: int<0, 65535>} */
  $vals = unpack("v", read_binary($data, $offset, 2));

  return $vals[1];
}

function read_uint32le($data, &$offset)
{
  /** @var array{1: int<0, max>} */
  $vals = unpack("V", read_binary($data, $offset, 4));

  return $vals[1];
}

function verb($message)
{
  global $opt_verbose;

  if ($opt_verbose)
    print $message . "\n";
}

function phar_fixup_timestamps($infile, $timestamp, $outfile)
{
  $SignatureAlgos = array(
    1 => array("md5", 16),
    2 => array("sha1", 20),
    3 => array("sha256", 32),
    4 => array("sha512", 64),
  );

  $contents = file_get_contents($infile);
  if ($contents === false)
    throw new \Exception("Cannot read file \"$infile\"");

  /* check signature magic code */
  if (substr($contents, -4) != "GBMB")
    throw new \Exception("Invalid phar data (bad signature magic)");

  /* check signature algorithm */
  list(, $signature_algo) = unpack("V", substr($contents, -8, 4));
  if (!isset($SignatureAlgos[$signature_algo]))
    throw new \Exception("Invalid phar data (unknown signature algorithm)");
  $siginfo = $SignatureAlgos[$signature_algo];

  /* check the actual signature */
  $signature_data = substr($contents, -($siginfo[1] + 8), $siginfo[1]);
  $signature_check = hash($siginfo[0], substr($contents, 0, -($siginfo[1] + 8)), true);

  if ($signature_data != $signature_check)
    throw new \Exception("Invalid phar data (bad signature)");

  /* detect manifest offset / end of stub */
  if (!preg_match('{__HALT_COMPILER\(\);(?: +\?>)?\r?\n}', $contents, $match,
      PREG_OFFSET_CAPTURE))
    throw new \Exception("Could not detect the stub's end in the phar");

  /* set starting position and skip past manifest length */
  $pos = $match[0][1] + strlen($match[0][0]);
  $stubEnd = $pos + read_uint32le($contents, $pos);

  $numFiles = read_uint32le($contents, $pos);

  // skip api version and flags
  $pos += 6;

  $aliasLength = read_uint32le($contents, $pos);
  $pos += $aliasLength;

  $metadataLength = read_uint32le($contents, $pos);
  $pos += $metadataLength;

  while ($pos < $stubEnd) {
    $filenameLength = read_uint32le($contents, $pos);
    $pos += $filenameLength;

    // skip filesize
    $pos += 4;

    // update timestamp to a fixed value
    $contents = substr_replace($contents, pack("V", $timestamp), $pos, 4);

    // skip timestamp, compressed file size, crc32 checksum and file flags
    $pos += 16;

    $metadataLength = read_uint32le($contents, $pos);
    $pos += $metadataLength;

    $numFiles--;
  }

  /* fix up the signature */
  $pos = strlen($contents) - ($siginfo[1] + 8);
  $signdata = hash($siginfo[0], substr($contents, 0, $pos), true);
  $contents = substr_replace($contents, $signdata, $pos, strlen($signdata));

  file_put_contents($outfile, $contents);
}

$local_args = $argv;
$progname = array_shift($local_args);

$infile = (string) array_shift($local_args);
$timestamp = (string) array_shift($local_args);
$outfile = (string) array_shift($local_args);
if (($infile == "") || ($timestamp == "")) {
  fprintf(STDERR, "Usage: %s <file.phar> <timestamp> [output.phar]\n", $progname);
  exit(1);
}

$opt_verbose = true;

if (preg_match('/^\d+$/', $timestamp)) {
  $timestamp = (int) $timestamp;
}
else {
  $timestamp = (int) strtotime($timestamp);
}

if ($outfile == "")
  $outfile = $infile;

print "Fixing up timestamps for '$infile' to '" .
    gmdate("Y-m-d H:i:s", $timestamp) . "' (UTC)\n";

try {
  phar_fixup_timestamps($infile, $timestamp, $outfile);
}
catch (\Exception $e) {
  fprintf(STDERR, "Error: %s\n", $e->getMessage());
  exit(1);
}
