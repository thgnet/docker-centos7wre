#!/bin/bash
#
# docker-centos7wre/builder/builder-files/build-all-php.sh
#
# Compiles PHP and generates a binary install package
#

set -eu

if [ -n "${1:-}" ]
then
  my_target="$1"
elif [ -n "${X_BUILDER_TARGET:-}" ]
then
  my_target="$X_BUILDER_TARGET"
else
  echo "Specify either target parameter or X_BUILDER_TARGET environment"
  exit 1
fi

if [ -n "${2:-}" ]
then
  my_action="$2"
else
  my_action="all"
fi

if [ "$my_target" = "all" ]
then
  selected_targets="php53 php56 php72 php74 php80 php81 php82 php83"
else
  selected_targets="${my_target/,/ }"
fi

for xtarget in $selected_targets
do
  if [ "$my_action" = "-refresh-patches" ]
  then
    /opt/builder-files/xbuild-php.sh $xtarget -prep
    /opt/builder-files/xbuild-php.sh $xtarget unpack
    /opt/builder-files/xbuild-php.sh $xtarget -patch
  else
    /opt/builder-files/xbuild-php.sh $xtarget $my_action
  fi
done
