#!/bin/bash
#
# docker-centos7wre/builder/builder-files/build-php53.inc.sh
#

php53.unpack.fixup() {
  :
}

php53.configure.args() {
  php_configure_args=(
    --prefix=/opt/php/$phptag
    --with-libdir=lib64
    --disable-cgi
    --with-apxs2=/usr/bin/apxs
    --with-openssl
    --with-zlib
    --enable-bcmath
    --with-bz2=shared
    --enable-calendar
    --with-curl
    --enable-dba=shared
    --with-db4=shared
    --enable-ftp
    --enable-exif
    --with-gd
    --with-jpeg-dir
    --with-freetype-dir
    --enable-gd-native-ttf
    --with-gettext
    --with-mhash
    --enable-shmop
    --enable-sysvshm
    --with-iconv
    --enable-intl=shared
    --enable-mbstring
    --with-mysql
    --with-mysqli
    --with-pdo-mysql
    --enable-pcntl
    --enable-dom
    --with-xmlrpc
    --with-mcrypt
    --enable-soap
    --enable-sockets
    --enable-zip
  )
  :
}

php53.configure.fixup() {
  # Ugly hack to skip apache config file modification
  sed -i 's/i -a -n php/i -n php/' Makefile
  :
}

php53.build.fixup() {
  :
}

php53.install.fixup() {
  mv /usr/lib64/httpd/modules/libphp5.so $installdir/lib/mod_php.so
  :
}

php53.pecl.install() {
  pecl_install zendopcache-7.0.5
  pecl_install APC-3.1.13
  pecl_install memcache-3.0.8
  pecl_install inotify-0.1.6
  :
}

php53.pear.install() {
  pear install "Mail"
  pear install "Mail_Mime"
  pear install "Auth_SASL"
  pear install "Net_SMTP"
  :
}

php53.package.fixup() {
  :
}
