#!/bin/bash

set -eu

new_version="${1:-}"

if [ -z "$new_version" ]
then
  echo "Usage: $0 <new-version>" >&2
  exit 1
fi

out() {
  echo -en "\n\n[+++] $1\n\n"
}

(
  cd builder/builder-files/php

  base_version="${new_version%.*}"

  # Remove all older files
  out "Removing old php-$base_version.x files"
  for xfile in php-$base_version.*.tar.bz2*
  do
    rm -v $xfile || :
  done

  # Attempt to download to validate the new version
  out "Attempting to download 'php-$new_version.tar.bz2'..."
  rm -f php-$new_version.tar.bz2
  rm -f php-$new_version.tar.bz2.asc
  wget \
      https://www.php.net/distributions/php-$new_version.tar.bz2 \
      https://www.php.net/distributions/php-$new_version.tar.bz2.asc

  # Generate the stamp file
  date -r php-$new_version.tar.bz2 --utc \
      "+%Y-%m-%dT%H:%M:%SZ" > php-$new_version.tar.bz2.stamp

  # Find an older patch file to upgrade
  old_patch=""
  for pfile in php-$base_version.*.patch
  do
    if [ "$pfile" = "php-$new_version.patch" ]
    then
      out "Already found a current patch for php-$new_version"
      exit 0
    else
      old_patch=$pfile
    fi
  done

  # Copy the old patch to the new one and attempt to refresh it
  if [ -n "$old_patch" ]
  then
    out "Copying patch file '$old_patch' to 'php-$new_version.patch' (you must refresh it manually!)"
    cp $old_patch php-$new_version.patch
  else
    out "No older patch file found for php-$base_version.x"
  fi
)

out "Operation completed"
